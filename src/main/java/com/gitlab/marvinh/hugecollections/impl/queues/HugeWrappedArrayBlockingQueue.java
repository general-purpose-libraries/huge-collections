package com.gitlab.marvinh.hugecollections.impl.queues;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeQueue;
import com.gitlab.marvinh.hugecollections.impl.iterators.LimitedSizeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Wrapper for Javas ArrayBlockingQueue
 */
public class HugeWrappedArrayBlockingQueue<V> implements HugeQueue<V> {
    private final Queue<V> queue;
    private final BigInteger capacity;
    private final boolean fair;

    public HugeWrappedArrayBlockingQueue(BigInteger capacity) {
        Misc.checkValue(capacity, BigInteger.valueOf(Integer.MAX_VALUE));
        this.capacity = capacity;
        fair = false;
        this.queue = new ArrayBlockingQueue<>(capacity.intValueExact(), false, new ArrayList<V>());
    }

    public HugeWrappedArrayBlockingQueue(BigInteger capacity, boolean fair) {
        Misc.checkValue(capacity, BigInteger.valueOf(Integer.MAX_VALUE));
        this.capacity = capacity;
        this.fair = fair;
        this.queue = new ArrayBlockingQueue<>(capacity.intValueExact(), fair, new ArrayList<V>());
    }

    public HugeWrappedArrayBlockingQueue(BigInteger capacity, boolean fair, HugeCollection<V> collection) {
        Misc.checkValue(capacity, BigInteger.valueOf(Integer.MAX_VALUE));
        Misc.checkCollectionSize(collection, BigInteger.valueOf(Integer.MAX_VALUE));
        this.capacity = capacity;
        this.fair = fair;
        this.queue = new ArrayBlockingQueue<>(capacity.intValueExact(), fair, collection.toCollection());
    }

    @Override
    public BigInteger size() {
        return BigInteger.valueOf(queue.size());
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public boolean contains(V element) {
        Misc.checkNotNull(element);
        return queue.contains(element);
    }

    @Override
    public boolean add(V element) {
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        return queue.add(element);
    }

    @Override
    public boolean remove(V element) {
        Misc.checkNotNull(element);
        return queue.remove(element);
    }

    @Override
    public boolean containsAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return queue.containsAll(elements.toCollection());
    }

    @Override
    public boolean addAll(HugeCollection<V> elements) {
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        return queue.addAll(elements.toCollection());
    }

    @Override
    public boolean removeAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return queue.removeAll(elements.toCollection());
    }

    @Override
    public boolean retainAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return queue.retainAll(elements.toCollection());
    }

    @Override
    public void clear() {
        queue.clear();
    }

    @Override
    public HugeCollectionIterator<V> iterator() {
        return new LimitedSizeCollectionIterator<>(queue);
    }

    @Override
    public V peek() {
        return queue.peek();
    }

    @Override
    public boolean offer(V element) {
        Misc.checkNotNull(element);
        return queue.offer(element);
    }

    @Override
    public V poll() {
        return queue.poll();
    }

    @Override
    public Queue<V> toQueue() {
        return new ArrayBlockingQueue<>(capacity.intValueExact(), fair, queue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HugeWrappedArrayBlockingQueue<?> that = (HugeWrappedArrayBlockingQueue<?>) o;

        return queue != null ? queue.equals(that.queue) : that.queue == null;
    }

    @Override
    public int hashCode() {
        return queue != null ? queue.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HugeWrappedArrayBlockingQueue{" +
                "queue=" + queue +
                ", capacity=" + capacity +
                ", fair=" + fair +
                '}';
    }
}
