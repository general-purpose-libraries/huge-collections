package com.gitlab.marvinh.hugecollections.impl.iterators;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This is an iterator for all implementations of
 * HugeCollection that are wrappers for java collections
 * and having a maximum size of Integer.MAX_VALUE
 */
public class LimitedSizeCollectionIterator<I> implements HugeCollectionIterator<I> {
    private final List<I> forwardQueue;
    private I currentElement;
    private final List<I> backwardQueue;
    private final Collection<I> collection;

    public LimitedSizeCollectionIterator(List<I> list, int index){
        Misc.checkCollectionIndex(list, index);
        forwardQueue = new LinkedList<>();
        currentElement = null;
        backwardQueue = new LinkedList<>();
        this.collection = list;
        for (int i = 0; i < index+1; i++){
            forwardQueue.add(list.get(i));
        }
        for (int i = index+1; i < list.size(); i++){
            backwardQueue.add(0, list.get(i));
        }
    }

    public LimitedSizeCollectionIterator(Collection<I> collection){
        Misc.checkCollectionSize(collection);
        forwardQueue = new LinkedList<>(collection);
        currentElement = null;
        backwardQueue = new LinkedList<>();
        this.collection = collection;
    }

    @Override
    public boolean hasPrevious() {
        return backwardQueue.size() > 0;
    }

    @Override
    public I previous() {
        if (!hasPrevious()){
            throw new IndexOutOfBoundsException("Iterator has no previous element.");
        }
        if (currentElement != null){
            forwardQueue.add(0, currentElement);
        }
        currentElement = backwardQueue.get(0);
        backwardQueue.remove(0);
        return currentElement;
    }

    @Override
    public HugeCollectionIterator<I> reverse() {
        LimitedSizeCollectionIterator<I> iterator = new LimitedSizeCollectionIterator<>(collection);
        if (this.backwardQueue.isEmpty() && this.currentElement == null){
            Collections.reverse(iterator.forwardQueue);
            return iterator;
        }
        iterator.forwardQueue.clear();
        iterator.forwardQueue.addAll(this.backwardQueue);
        iterator.backwardQueue.clear();
        iterator.backwardQueue.addAll(this.forwardQueue);
        return iterator;
    }

    @Override
    public void remove() {
        if (currentElement == null){
            throw new IndexOutOfBoundsException("Currently no element is selected which can be removed.");
        }
        collection.remove(currentElement);
        currentElement = null;
    }

    @Override
    public boolean hasNext() {
        return forwardQueue.size() > 0;
    }

    @Override
    public I next() {
        if (!hasNext()){
            throw new IndexOutOfBoundsException("Iterator has no next element.");
        }
        if (currentElement != null){
            backwardQueue.add(0, currentElement);
        }
        currentElement = forwardQueue.get(0);
        forwardQueue.remove(0);
        return currentElement;
    }
}
