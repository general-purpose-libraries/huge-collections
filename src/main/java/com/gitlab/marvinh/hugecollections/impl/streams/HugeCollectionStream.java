package com.gitlab.marvinh.hugecollections.impl.streams;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.*;
import java.util.stream.*;

/**
 * Created by chief on 10.04.17.
 */
public class HugeCollectionStream<V> implements Stream<V> {
    private final HugeCollection<V> collection;

    public HugeCollectionStream(HugeCollection<V> collection) {
        Misc.checkNotNull(collection);
        this.collection = collection;
    }

    @Override
    public Stream<V> filter(Predicate<? super V> predicate) {
        return null;
    }

    @Override
    public <R> Stream<R> map(Function<? super V, ? extends R> function) {
        return null;
    }

    @Override
    public IntStream mapToInt(ToIntFunction<? super V> toIntFunction) {
        return null;
    }

    @Override
    public LongStream mapToLong(ToLongFunction<? super V> toLongFunction) {
        return null;
    }

    @Override
    public DoubleStream mapToDouble(ToDoubleFunction<? super V> toDoubleFunction) {
        return null;
    }

    @Override
    public <R> Stream<R> flatMap(Function<? super V, ? extends Stream<? extends R>> function) {
        return null;
    }

    @Override
    public IntStream flatMapToInt(Function<? super V, ? extends IntStream> function) {
        return null;
    }

    @Override
    public LongStream flatMapToLong(Function<? super V, ? extends LongStream> function) {
        return null;
    }

    @Override
    public DoubleStream flatMapToDouble(Function<? super V, ? extends DoubleStream> function) {
        return null;
    }

    @Override
    public Stream<V> distinct() {
        return null;
    }

    @Override
    public Stream<V> sorted() {
        return null;
    }

    @Override
    public Stream<V> sorted(Comparator<? super V> comparator) {
        return null;
    }

    @Override
    public Stream<V> peek(Consumer<? super V> consumer) {
        return null;
    }

    @Override
    public Stream<V> limit(long l) {
        return null;
    }

    @Override
    public Stream<V> skip(long l) {
        return null;
    }

    @Override
    public void forEach(Consumer<? super V> consumer) {

    }

    @Override
    public void forEachOrdered(Consumer<? super V> consumer) {

    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <A> A[] toArray(IntFunction<A[]> intFunction) {
        return null;
    }

    @Override
    public V reduce(V v, BinaryOperator<V> binaryOperator) {
        return null;
    }

    @Override
    public Optional<V> reduce(BinaryOperator<V> binaryOperator) {
        return null;
    }

    @Override
    public <U> U reduce(U u, BiFunction<U, ? super V, U> biFunction, BinaryOperator<U> binaryOperator) {
        return null;
    }

    @Override
    public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super V> biConsumer, BiConsumer<R, R> biConsumer1) {
        return null;
    }

    @Override
    public <R, A> R collect(Collector<? super V, A, R> collector) {
        return null;
    }

    @Override
    public Optional<V> min(Comparator<? super V> comparator) {
        return null;
    }

    @Override
    public Optional<V> max(Comparator<? super V> comparator) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public boolean anyMatch(Predicate<? super V> predicate) {
        return false;
    }

    @Override
    public boolean allMatch(Predicate<? super V> predicate) {
        return false;
    }

    @Override
    public boolean noneMatch(Predicate<? super V> predicate) {
        return false;
    }

    @Override
    public Optional<V> findFirst() {
        return null;
    }

    @Override
    public Optional<V> findAny() {
        return null;
    }

    @Override
    public Iterator<V> iterator() {
        return null;
    }

    @Override
    public Spliterator<V> spliterator() {
        return null;
    }

    @Override
    public boolean isParallel() {
        return false;
    }

    @Override
    public Stream<V> sequential() {
        return null;
    }

    @Override
    public Stream<V> parallel() {
        return null;
    }

    @Override
    public Stream<V> unordered() {
        return null;
    }

    @Override
    public Stream<V> onClose(Runnable runnable) {
        return null;
    }

    @Override
    public void close() {

    }
}
