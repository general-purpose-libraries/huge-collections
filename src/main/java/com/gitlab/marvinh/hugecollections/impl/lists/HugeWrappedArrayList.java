package com.gitlab.marvinh.hugecollections.impl.lists;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeList;
import com.gitlab.marvinh.hugecollections.impl.iterators.LimitedSizeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.UnaryOperator;

/**
 * This is a wrapper class for an ArrayList.
 * Its size is limited to Integer.MAX_VALUE.
 */
public class HugeWrappedArrayList<V> implements HugeList<V> {
    private final ArrayList<V> list;

    public HugeWrappedArrayList(){
        list = new ArrayList<>();
    }

    public HugeWrappedArrayList(HugeCollection<V> collection) {
        Misc.checkCollectionSize(collection, BigInteger.valueOf(Integer.MAX_VALUE));
        this.list = new ArrayList<>(collection.toCollection());
    }

    public HugeWrappedArrayList(Collection<V> collection){
        Misc.checkCollectionSize(collection);
        this.list = new ArrayList<>(collection);
    }

    @Override
    public BigInteger size() {
        return BigInteger.valueOf(this.list.size());
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public boolean contains(V element) {
        Misc.checkNotNull(element);
        return this.list.contains(element);
    }

    @Override
    public boolean add(V element) {
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.add(element);
    }

    @Override
    public boolean remove(V element) {
        Misc.checkNotNull(element);
        return this.list.remove(element);
    }

    @Override
    public boolean containsAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.containsAll(elements.toCollection());
    }

    @Override
    public boolean addAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(this, elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.addAll(elements.toCollection());
    }

    @Override
    public boolean removeAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.removeAll(elements.toCollection());
    }

    @Override
    public boolean retainAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.retainAll(elements.toCollection());
    }

    @Override
    public void clear() {
        this.list.clear();
    }

    @Override
    public HugeCollectionIterator<V> iterator() {
        return new LimitedSizeCollectionIterator<>(this.list);
    }

    @Override
    public boolean add(BigInteger index, V element) {
        Misc.checkCollectionIndexForAdd(this, index, BigInteger.valueOf(Integer.MAX_VALUE));
        Misc.checkNotNull(element);
        try{
            this.list.add(index.intValueExact(), element);
        }
        catch (IllegalArgumentException e){
            return false;
        }
        return true;
    }

    @Override
    public boolean addAll(BigInteger index, HugeCollection<V> elements) {
        Misc.checkCollectionSize(this, elements, BigInteger.valueOf(Integer.MAX_VALUE));
        Misc.checkCollectionIndexForAdd(this, index, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.list.addAll(index.intValueExact(), elements.toCollection());
    }

    @Override
    public V get(BigInteger index) {
        Misc.checkCollectionIndexForGet(this, index);
        return this.list.get(index.intValueExact());
    }

    @Override
    public BigInteger indexOf(V element) {
        Misc.checkNotNull(element);
        int index = this.list.indexOf(element);
        if (index < 0){
            throw new NoSuchElementException("List does not contain the given element.");
        }
        return BigInteger.valueOf(index);
    }

    @Override
    public BigInteger lastIndexOf(V element) {
        Misc.checkNotNull(element);
        int index = this.list.lastIndexOf(element);
        if (index < 0){
            throw new NoSuchElementException("List does not contain the given element.");
        }
        return BigInteger.valueOf(index);
    }

    @Override
    public HugeCollectionIterator<V> iterator(BigInteger index) {
        Misc.checkCollectionIndexForGet(this, index);
        return new LimitedSizeCollectionIterator<>(this.list, index.intValueExact());
    }

    @Override
    public void replaceAll(UnaryOperator<V> operator) {
        assert operator != null;
        this.list.replaceAll(operator);
    }

    @Override
    public V set(BigInteger index, V element) {
        Misc.checkCollectionIndexForGet(this, index);
        Misc.checkNotNull(element);
        return this.list.set(index.intValueExact(), element);
    }

    @Override
    public HugeList<V> subList(BigInteger fromIndex, BigInteger toIndex) {
        Misc.checkCollectionIndex(this, fromIndex, toIndex);
        List<V> subList = this.list.subList(fromIndex.intValueExact(), toIndex.intValueExact());
        HugeWrappedArrayList<V> hugeWrappedArrayList = new HugeWrappedArrayList<>();
        for (V element : subList){
            hugeWrappedArrayList.add(element);
        }
        return hugeWrappedArrayList;
    }

    @Override
    public List<V> toList() {
        return new ArrayList<>(this.list);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HugeWrappedArrayList<?> that = (HugeWrappedArrayList<?>) o;

        return list != null ? list.equals(that.list) : that.list == null;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HugeWrappedArrayList{" +
                list +
                '}';
    }
}
