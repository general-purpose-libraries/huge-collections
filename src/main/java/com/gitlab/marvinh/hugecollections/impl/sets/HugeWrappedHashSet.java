package com.gitlab.marvinh.hugecollections.impl.sets;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeSet;
import com.gitlab.marvinh.hugecollections.impl.iterators.LimitedSizeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * /**
 * This is a wrapper class for a HashSet.
 * Its size is limited to Integer.MAX_VALUE.
 */
public class HugeWrappedHashSet<V> implements HugeSet<V> {
    private final Set<V> set;

    public HugeWrappedHashSet(){
        this.set = new HashSet<>();
    }

    public HugeWrappedHashSet(Collection<V> collection) {
        Misc.checkCollectionSize(collection);
        this.set = new HashSet<>(collection);
    }

    public HugeWrappedHashSet(HugeCollection<V> collection){
        Misc.checkCollectionSize(collection, BigInteger.valueOf(Integer.MAX_VALUE));
        this.set = new HashSet<>(collection.toCollection());
    }

    @Override
    public BigInteger size() {
        return BigInteger.valueOf(set.size());
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(V element) {
        Misc.checkNotNull(element);
        return set.contains(element);
    }

    @Override
    public boolean add(V element) {
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.set.add(element);
    }

    @Override
    public boolean remove(V element) {
        Misc.checkNotNull(element);
        return this.set.remove(element);
    }

    @Override
    public boolean containsAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.set.containsAll(elements.toCollection());
    }

    @Override
    public boolean addAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(this, elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.set.addAll(elements.toCollection());
    }

    @Override
    public boolean removeAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.set.removeAll(elements.toCollection());
    }

    @Override
    public boolean retainAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.set.retainAll(elements.toCollection());
    }

    @Override
    public void clear() {
        this.set.clear();
    }

    @Override
    public HugeCollectionIterator<V> iterator() {
        return new LimitedSizeCollectionIterator<>(this.set);
    }

    @Override
    public Set<V> toSet() {
        return new HashSet<>(set);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HugeWrappedHashSet<?> that = (HugeWrappedHashSet<?>) o;

        return set != null ? set.equals(that.set) : that.set == null;
    }

    @Override
    public int hashCode() {
        return set != null ? set.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HugeWrappedHashSet{" +
                "set=" + set +
                '}';
    }
}
