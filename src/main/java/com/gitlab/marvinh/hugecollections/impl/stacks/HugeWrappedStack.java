package com.gitlab.marvinh.hugecollections.impl.stacks;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeStack;
import com.gitlab.marvinh.hugecollections.impl.iterators.LimitedSizeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;

/**
 * This is a wrapper class for a Stack.
 * Its size is limited to Integer.MAX_VALUE.
 */
public class HugeWrappedStack<V> implements HugeStack<V> {
    private final Deque<V> stack;

    public HugeWrappedStack() {
        stack = new ArrayDeque<>();
    }

    public HugeWrappedStack(HugeCollection<V> collection){
        Misc.checkCollectionSize(collection, BigInteger.valueOf(Integer.MAX_VALUE));
        stack = new ArrayDeque<>(collection.toCollection());
    }

    public HugeWrappedStack(Collection<V> collection){
        Misc.checkCollectionSize(collection);
        stack = new ArrayDeque<>(collection);
    }

    @Override
    public V pop() {
        return this.stack.removeFirst();
    }

    @Override
    public V push(V element) {
        Misc.checkNotNull(element);
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        this.stack.addFirst(element);
        return element;
    }

    @Override
    public Deque<V> toDeque() {
        Deque<V> stack = new ArrayDeque<>();
        for (V element : this.stack){
            stack.addLast(element);
        }
        return stack;
    }

    @Override
    public BigInteger size() {
        return BigInteger.valueOf(this.stack.size());
    }

    @Override
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }

    @Override
    public boolean contains(V element) {
        Misc.checkNotNull(element);
        return this.stack.contains(element);
    }

    @Override
    public boolean add(V element) {
        Misc.checkNotNull(element);
        Misc.checkCollectionSizeForAdd(this, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.stack.add(element);
    }

    @Override
    public boolean remove(V element) {
        Misc.checkNotNull(element);
        return this.stack.remove(element);
    }

    @Override
    public boolean containsAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.stack.containsAll(elements.toCollection());
    }

    @Override
    public boolean addAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(this, elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.stack.addAll(elements.toCollection());
    }

    @Override
    public boolean removeAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.stack.removeAll(elements.toCollection());
    }

    @Override
    public boolean retainAll(HugeCollection<V> elements) {
        Misc.checkCollectionSize(elements, BigInteger.valueOf(Integer.MAX_VALUE));
        return this.stack.retainAll(elements.toCollection());
    }

    @Override
    public void clear() {
        this.stack.clear();
    }

    @Override
    public Collection<V> toCollection() {
        return toDeque();
    }

    @Override
    public HugeCollectionIterator<V> iterator() {
        return new LimitedSizeCollectionIterator<>(this.stack);
    }

    @Override
    public V peek() {
        return this.stack.peekFirst();
    }
}
