package com.gitlab.marvinh.hugecollections.impl.lists;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeList;
import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.UnaryOperator;

/**
 * Implementation of a doubly Linked List
 */
public class HugeLinkedList<V> implements HugeList<V> {
    protected BigInteger size;

    protected class ListEntry<E> {
        private E element;
        private ListEntry<E> previous;
        private ListEntry<E> next;

        private ListEntry(){
            this.element = null;
            this.next = null;
            this.previous = null;
        }

        private ListEntry(E element, ListEntry<E> previous, ListEntry<E> next) {
            Misc.checkNotNull(element);
            this.element = element;
            this.previous = previous;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            Misc.checkNotNull(element);
            this.element = element;
        }

        @Override
        public String toString() {
            return "ListEntry{" +
                    "element=" + element +
                    '}';
        }
    }

    private class HugeLinkedListIterator<E> implements HugeCollectionIterator<E> {
        private final HugeLinkedList<E> list;
        private HugeLinkedList<E>.ListEntry<E> current;
        private boolean moveForward;

        private HugeLinkedListIterator(HugeLinkedList<E> list, boolean moveForward) {
            assert list != null;
            this.list = list;
            this.moveForward = moveForward;
            this.current = list.anchor;
        }

        public HugeLinkedListIterator(HugeLinkedList<E> list) {
            assert list != null;
            this.list = list;
            this.moveForward = true;
            this.current = list.anchor;
        }

        public HugeLinkedListIterator(HugeLinkedList<E> list, BigInteger index) {
            this(list);
            assert index != null;
            BigInteger i = BigInteger.ZERO;
            while (hasNext() && i.compareTo(index) < 0){
                next();
                i = i.add(BigInteger.ONE);
            }
        }

        @Override
        public boolean hasPrevious() {
            if (moveForward){
                return current.previous != null;
            }
            return current.next != null;
        }

        @Override
        public E previous() {
            if (!hasPrevious()){
                throw new NoSuchElementException("There is no previous element");
            }
            if (moveForward){
                current = current.previous;
            }
            else {
                current = current.next;
            }
            return current.element;
        }

        HugeLinkedList<E>.ListEntry<E> getCurrent(){
            return this.current;
        }

        HugeLinkedList<E>.ListEntry<E> getPrevious(){
            return this.current.previous;
        }

        @Override
        public HugeCollectionIterator<E> reverse() {
            return new HugeLinkedListIterator<>(this.list, false);
        }

        @Override
        public boolean hasNext() {
            if (moveForward){
                return current.next != null;
            }
            return current.previous != null;
        }

        @Override
        public E next() {
            if (!hasNext()){
                throw new NoSuchElementException("There is no next element");
            }
            if (moveForward){
                current = current.next;
            }
            else {
                current = current.previous;
            }
            return current.element;
        }

        @Override
        public void remove() {
            HugeLinkedList<E>.ListEntry<E> previous;
            HugeLinkedList<E>.ListEntry<E> next;

            if (current.previous != null){
                previous = current.previous;
                if (hasNext()){
                    next = current.next;
                    previous.next = next;
                    next.previous = previous;
                }
                else {
                    previous.next = null;
                }
                assert list.size.compareTo(BigInteger.ZERO) > 0;
                this.list.size = this.list.size.subtract(BigInteger.ONE);
            }
            else {
                if (current.next != null){
                    HugeLinkedList<E>.ListEntry<E> nextEntry = current.next;
                    assert this.list.size.compareTo(BigInteger.ZERO) > 0;
                    this.list.size = this.list.size.subtract(BigInteger.ONE);
                    this.list.anchor.next = nextEntry;
                    nextEntry.previous = null;
                    return;
                }
                this.list.anchor.next = null;
                this.list.size = BigInteger.ZERO;
                this.list.anchor.previous = null;
            }
        }
    }

    protected final ListEntry<V> anchor;

    public HugeLinkedList() {
        this.anchor = new ListEntry<>();
        this.size = BigInteger.ZERO;
    }

    public HugeLinkedList(HugeCollection<V> collection){
        this();
        Misc.checkNotNull(collection);
        addAll(collection);
    }

    public HugeLinkedList(Collection<V> collection){
        this();
        Misc.checkNotNull(collection);
        for (V element : collection){
            add(element);
        }
    }

    @Override
    public BigInteger size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return anchor.next == null;
    }

    @Override
    public boolean contains(V element) {
        Misc.checkNotNull(element);
        for (V current : this) {
            if (current.equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(V element) {
        Misc.checkNotNull(element);
        if (size.compareTo(BigInteger.ZERO) == 0){
            ListEntry<V> entry = new ListEntry<>(element, null, null);
            anchor.next = entry;
            anchor.previous = entry;
            size = size.add(BigInteger.ONE);
            return true;
        }
        ListEntry<V> entry = new ListEntry<>(element, anchor.previous, null);
        anchor.previous.next = entry;
        anchor.previous = entry;
        size = size.add(BigInteger.ONE);
        return true;
    }

    @Override
    public boolean remove(V element) {
        Misc.checkNotNull(element);
        HugeCollectionIterator<V> iterator = iterator();
        while (iterator.hasNext()){
            if (iterator.next().equals(element)){
                iterator.remove();
                return true;
            }
        }
        throw new NoSuchElementException("List does not contain given element");
    }

    @Override
    public boolean containsAll(HugeCollection<V> elements) {
        Misc.checkNotNull(elements);
        for (V element : elements){
            if (!this.contains(element)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(HugeCollection<V> elements) {
        Misc.checkNotNull(elements);
        for (V element : elements){
            this.add(element);
        }
        return true;
    }

    @Override
    public boolean removeAll(HugeCollection<V> elements) {
        Misc.checkNotNull(elements);
        BigInteger elementsProcessed = BigInteger.ZERO;
        HugeCollectionIterator<V> iterator = iterator();
        while (iterator.hasNext()){
            V element = iterator.next();
            if (elements.contains(element)){
                iterator.remove();
                elementsProcessed.add(BigInteger.ONE);
            }
        }
        return elementsProcessed.compareTo(BigInteger.ZERO) > 0;
    }

    @Override
    public boolean retainAll(HugeCollection<V> elements) {
        Misc.checkNotNull(elements);
        BigInteger elementsProcessed = BigInteger.ZERO;
        HugeCollectionIterator<V> iterator = iterator();
        while (iterator.hasNext()){
            V element = iterator.next();
            if (!elements.contains(element)){
                iterator.remove();
                elementsProcessed.add(BigInteger.ONE);
            }
        }
        return elementsProcessed.compareTo(BigInteger.ZERO) > 0;
    }

    @Override
    public void clear() {
        HugeCollectionIterator<V> iterator = iterator();
        while (iterator.hasNext()){
            iterator.next();
            iterator.remove();
        }
    }

    @Override
    public HugeCollectionIterator<V> iterator() {
        return new HugeLinkedListIterator<>(this);
    }

    @Override
    public boolean add(BigInteger index, V element) {
        Misc.checkNotNull(index, element);
        Misc.checkCollectionIndexForAdd(this, index);
        BigInteger i = BigInteger.ZERO;
        HugeLinkedListIterator<V> iterator = (HugeLinkedListIterator<V>) iterator();
        while (iterator.hasNext()){
            assert i.compareTo(index) <= 0;
            iterator.next();
            if (i.compareTo(index) == 0){
                break;
            }
        }
        ListEntry<V> current = iterator.getCurrent();
        if (iterator.hasPrevious()){
            ListEntry<V> entry = new ListEntry<>(element, iterator.getPrevious(), current);
            iterator.getPrevious().next = entry;
        }
        else {
            anchor.next = new ListEntry<>(element, null, current);
        }
        size = size.add(BigInteger.ONE);
        return true;
    }

    @Override
    public boolean addAll(BigInteger index, HugeCollection<V> elements) {
        Misc.checkNotNull(index, elements);
        Misc.checkCollectionIndexForAdd(this, index);
        HugeCollectionIterator<V> iterator = elements.iterator().reverse();
        while (iterator.hasNext()){
            this.add(index, iterator.next());
        }
        return true;
    }

    @Override
    public V get(BigInteger index) {
        Misc.checkCollectionIndexForGet(this, index);
        BigInteger i = BigInteger.ZERO;
        HugeCollectionIterator<V> iterator = iterator();
        while (iterator.hasNext() && i.compareTo(index) < 0){
            iterator.next();
            i = i.add(BigInteger.ONE);
        }
        return iterator.next();
    }

    @Override
    public BigInteger indexOf(V element) {
        Misc.checkNotNull(element);
        BigInteger index = BigInteger.ZERO;
        for (V elem : this){
            if (elem.equals(element)){
                return index;
            }
            index = index.add(BigInteger.ONE);
        }
        throw new NoSuchElementException("List does not contain given element");
    }

    @Override
    public BigInteger lastIndexOf(V element) {
        Misc.checkNotNull(element);
        BigInteger index = size().subtract(BigInteger.ONE);
        HugeCollectionIterator<V> iterator = iterator().reverse();
        while (iterator.hasNext()){
            V elem = iterator.next();
            if (elem.equals(element)){
                return index;
            }
            index = index.subtract(BigInteger.ONE);
        }
        throw new NoSuchElementException("List does not contain given element");
    }

    @Override
    public HugeCollectionIterator<V> iterator(BigInteger index) {
        Misc.checkNotNull(index);
        return new HugeLinkedListIterator<>(this, index);
    }

    @Override
    public void replaceAll(UnaryOperator<V> operator) {
        Misc.checkNotNull(operator);
        HugeLinkedListIterator<V> iterator = new HugeLinkedListIterator<V>(this);
        while (iterator.hasNext()){
            iterator.next();
            ListEntry<V> entry = iterator.getCurrent();
            entry.setElement(operator.apply(entry.getElement()));
        }
    }

    @Override
    public V set(BigInteger index, V element) {
        Misc.checkNotNull(index, element);
        HugeLinkedListIterator<V> iterator = new HugeLinkedListIterator<>(this, index);
        iterator.next();
        ListEntry<V> entry = iterator.getCurrent();
        V prevElement = entry.element;
        entry.element = element;
        return prevElement;
    }

    @Override
    public HugeList<V> subList(BigInteger fromIndex, BigInteger toIndex) {
        Misc.checkCollectionIndex(this, fromIndex, toIndex);
        HugeList<V> list = new HugeLinkedList<>();
        HugeLinkedListIterator<V> iterator = new HugeLinkedListIterator<>(this, fromIndex);
        BigInteger index = fromIndex;
        while (index.compareTo(toIndex) < 0){
            if (!iterator.hasNext()){
                throw new NoSuchElementException(String.format("This list does not contain the given index: %s", toIndex));
            }
            list.add(iterator.next());
            index = index.add(BigInteger.ONE);
        }
        return list;
    }

    @Override
    public List<V> toList() {
        List<V> list = new LinkedList<>();
        for (V element : this){
            list.add(element);
        }
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HugeLinkedList<?> that = (HugeLinkedList<?>) o;

        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        HugeCollectionIterator<V> iterator = iterator();
        HugeCollectionIterator<?> iterator2 = that.iterator();
        while (iterator.hasNext() && iterator2.hasNext()){
            if (!iterator.next().equals(iterator2.next())){
                return false;
            }
        }
        assert !iterator.hasNext() && !iterator2.hasNext();
        return true;
    }

    @Override
    public int hashCode() {
        int result = size != null ? size.hashCode() : 0;
        result = 31 * result + (anchor != null ? anchor.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HugeLinkedList{" +
                "size=" + size +
                ", anchor=" + anchor +
                '}';
    }
}
