package com.gitlab.marvinh.hugecollections.impl.utils;

import com.gitlab.marvinh.hugecollections.api.HugeCollection;
import com.gitlab.marvinh.hugecollections.impl.lists.HugeWrappedArrayList;

import java.math.BigInteger;
import java.util.Collection;

/**
 * Contains some general purpose functions
 */
public class Misc {
    public static <I> void checkCollectionIndexForGet(HugeCollection<I> collection, BigInteger index){
        checkNotNull(collection, index);
        if (index.compareTo(BigInteger.ZERO) < 0){
            throw new IndexOutOfBoundsException("Negative index values are not allowed.");
        }
        if (index.compareTo(collection.size()) >= 0){
            throw new IndexOutOfBoundsException("Index can not be greater or equal to collection size");
        }
    }

    public static <I> void checkCollectionIndexForAdd(HugeCollection<I> collection, BigInteger index, BigInteger maxCollectionSize){
        checkCollectionIndexForAdd(collection, index);
        if (index.compareTo(maxCollectionSize) > 0){
            throw new IndexOutOfBoundsException(String.format("Collection does not support more than %s elements", maxCollectionSize));
        }
    }

    public static <I> void checkCollectionIndex(HugeCollection<I> collection, BigInteger fromIndex, BigInteger toIndex){
        checkNotNull(collection, fromIndex, toIndex);
        if (fromIndex.compareTo(BigInteger.ZERO) < 0){
            throw new IndexOutOfBoundsException("Start index must not be negative");
        }
        if (fromIndex.compareTo(toIndex) > 0){
            throw new IndexOutOfBoundsException("Start index must not be greater than end index");
        }
    }

    public static <I> void checkCollectionIndexForAdd(HugeCollection<I> collection, BigInteger index){
        checkNotNull(collection, index);
        if (index.compareTo(BigInteger.ZERO) < 0){
            throw new IndexOutOfBoundsException("Negative index values are not allowed.");
        }
        if (index.compareTo(collection.size()) > 0){
            throw new IndexOutOfBoundsException("Index can not be greater than collection size");
        }
    }

    public static <I> void checkCollectionSizeForAdd(HugeCollection<I> collection, BigInteger maxSize){
        checkNotNull(collection, maxSize);
        if (collection.size().add(BigInteger.ONE).compareTo(maxSize) > 0){
            throw new RuntimeException(String.format("Collection does not allow more than %s elements", maxSize));
        }
    }

    public static <I> void checkCollectionSize(HugeCollection<I> collection, BigInteger maxSize){
        checkNotNull(collection, maxSize);
        if (collection.size().compareTo(maxSize) > 0){
            throw new RuntimeException(String.format("Collection does not allow more than %s elements", maxSize));
        }
    }

    public static <I> void checkCollectionSize(HugeCollection<I> collection1, HugeCollection<I> collection2, BigInteger maxSize){
        checkNotNull(collection1, collection2, maxSize);
        if (collection1.size().add(collection2.size()).compareTo(maxSize) > 0){
            throw new RuntimeException(String.format("Combined collectiion has more than %s elements", maxSize));
        }
    }

    public static <I> void checkCollectionSize(Collection<I> collection){
        checkNotNull(collection);
        long counter = 0;
        for (I ignored : collection){
            if (counter >= Integer.MAX_VALUE){
                throw new IndexOutOfBoundsException(String.format("%s does not support more than %s elements",
                        HugeWrappedArrayList.class.getSimpleName(),  "Integer.MAX_VALUE"));
            }
            counter++;
        }
    }

    public static <I> void checkCollectionIndex(Collection<I> collection, int index){
        checkNotNull(collection, index);
        long counter = 0;
        for (I ignored : collection){
            if (counter >= Integer.MAX_VALUE){
                throw new IndexOutOfBoundsException(String.format("%s does not support more than %s elements",
                        HugeWrappedArrayList.class.getSimpleName(),  "Integer.MAX_VALUE"));
            }
            counter++;
        }
        if (index < 0){
            throw new IndexOutOfBoundsException("Negative index numbers are not allowed.");
        }
        if (index >= counter){
            throw new IndexOutOfBoundsException("This list does not contain the given index number.");
        }
    }

    public static void checkNotNull(Object... o){
        for (Object obj : o){
            if (obj == null){
                throw new RuntimeException("Null pointer not permitted here");
            }
        }
    }

    public static void checkValue(BigInteger valueToCheck, BigInteger maxValue){
        if (valueToCheck.compareTo(BigInteger.ZERO) < 0){
            throw new IllegalArgumentException("Value must not be negative");
        }
        if (valueToCheck.compareTo(maxValue) > 0){
            throw new IllegalArgumentException(String.format("Value can not be greater than %s", maxValue));
        }
    }
}
