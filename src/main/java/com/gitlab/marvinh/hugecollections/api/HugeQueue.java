package com.gitlab.marvinh.hugecollections.api;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Created by chief on 21.03.17.
 */
public interface HugeQueue<V> extends HugeCollection<V>{
    /**
     * Returns the head of this queue, but does not remove it from the queue
     * @return The head of this queue
     * @throws NoSuchElementException If the queue is empty
     */
    V peek();

    /**
     * Adds an element to the queue
     * @param element Element to be added to the queue
     * @return True, if the element has been added sucessfully. Else if not.
     */
    boolean offer(V element);

    /**
     * Returns the head of this queue and removes it from the queue
     * @return The head of this queue
     */
    V poll();

    @Override
    default Collection<V> toCollection(){
        return toQueue();
    }

    /**
     * Transforms this set into a java.lang.Set
     * @return The transformed set
     */
    Queue<V> toQueue();
}
