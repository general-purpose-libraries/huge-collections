package com.gitlab.marvinh.hugecollections.api;

import java.util.Comparator;

/**
 * This interface represents an algorithm that can be applied to a collection
 * @param <C> the collecction type
 * @param <E> the element type of the collection
 */
public interface Algorithm<C extends HugeCollection<E>, E> {
    /**
     * Applies the algorithm to the given collection
     * @param collection collection to be processed
     * @param comparator comparator for comparing elements of the collection
     */
    void apply(C collection, Comparator<E> comparator);
}
