package com.gitlab.marvinh.hugecollections.api;

import java.util.Collection;
import java.util.Set;

/**
 * Created by chief on 21.03.17.
 */
public interface HugeSet<V> extends HugeCollection<V> {
    @Override
    default Collection<V> toCollection(){
        return toSet();
    }

    /**
     * Transforms this set into a java.lang.Set
     * @return The transformed set
     */
    Set<V> toSet();
}
