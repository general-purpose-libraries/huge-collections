package com.gitlab.marvinh.hugecollections.api;

import java.util.Iterator;

/**
 * This interface represents iterators for elements of a HugeCollection
 */
public interface HugeCollectionIterator<V> extends Iterator<V> {
    /**
     * Checks if the iterator could go one step backward.
     * @return True, if the iterator has a previous element. False else.
     */
    boolean hasPrevious();

    /**
     * Goes one step backward.
     * @return Previous element
     */
    V previous();

    /**
     * @return Iterator iterating through all elements of this iterator in reverse order
     */
    HugeCollectionIterator<V> reverse();

    /**
     * Removes the current element from the iterator and the iterated collection
     */
    @Override
    void remove();
}
