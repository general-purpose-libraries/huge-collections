package com.gitlab.marvinh.hugecollections.api;

import java.util.Collection;
import java.util.Deque;
import java.util.NoSuchElementException;

/**
 * This interface represents stacks
 */
public interface HugeStack<V> extends HugeCollection<V> {

    /**
     * Remove the first element of the stack
     * @return The element having been the first element of the stack
     */
    V pop();

    /**
     * Places the given element on top of the stack
     * @return The given element
     */
    V push(V element);

    /**
     * Returns the head of this queue, but does not remove it from the stack
     * @return The head of this stack
     * @throws NoSuchElementException If the stack is empty
     */
    V peek();

    @Override
    default Collection<V> toCollection(){
        return toDeque();
    }

    /**
     * Transforms this stack into a java.lang.Deque
     * @return The transformed deque
     */
    Deque<V> toDeque();
}
