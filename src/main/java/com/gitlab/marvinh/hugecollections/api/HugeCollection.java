package com.gitlab.marvinh.hugecollections.api;

import com.gitlab.marvinh.hugecollections.impl.utils.Misc;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Comparator;

/**
 * Interface for a collection with infinite size
 * @param <V> type of objects stored in the collection
 */
public interface HugeCollection<V> extends Iterable<V>{
    /**
     * @return Size of this collection
     */
    BigInteger size();

    /**
     * @return True, if the collection is empty. False else.
     */
    boolean isEmpty();

    /**
     * Checks whether the given element exists in this collection
     * @param element Element to be checked
     * @return True, if the element is contained in this collection. False else.
     */
    boolean contains(V element);

    /**
     * Adds the given element to this collection
     * @param element Element to be added
     * @return True, if the list has been changed by the function call. False else.
     */
    boolean add(V element);

    /**
     * Removes the given element from this collection
     * @param element Element to be removed
     * @return True, if the list has been changed by the function call. False else.
     */
    boolean remove(V element);

    /**
     * Checks whether this collection has all elements of the given collection
     * @param elements The collection to be checked
     * @return True, if this collections contains all elements of the given collection. False else.
     */
    boolean containsAll(HugeCollection<V> elements);

    /**
     * Adds all elements of the given collection to this collection
     * @param elements Elements to be added to this collection
     * @return True, if the list has been changed by the function call. False else.
     */
    boolean addAll(HugeCollection<V> elements);

    /**
     * Removes all elements of the given collection from this collection
     * @param elements Elements to be removed from this collection
     * @return True, if the list has been changed by the function call. False else.
     */
    boolean removeAll(HugeCollection<V> elements);

    /**
     * Removes all elements from this list that are not contained in the given list
     * @param elements Elements to retain
     * @return True, if the list has been changed by the function call. False else.
     */
    boolean retainAll(HugeCollection<V> elements);

    /**
     * Removes all elements from this collection
     */
    void clear();

    /**
     * Transforms this collection into a java.lang.Collection
     * @return The transformed collection
     */
    Collection<V> toCollection();

    /**
     * @return Iterator over all elements in the collection
     */
    @Override
    HugeCollectionIterator<V> iterator();

    /**
     * Applies an algorithm to this collection
     * @param comparator compares the elements of this collection
     * @param algorithm algorithm to be applied
     */
    default void apply(Comparator<V> comparator, Algorithm<HugeCollection<V>, V> algorithm){
        Misc.checkNotNull(comparator, algorithm);
        algorithm.apply(this, comparator);
    }

    /**
     * @param o Object to be compared to the collection
     * @return True if the object is equal to the collection
     */
    @Override
    boolean equals(Object o);

    /**
     * @return Hashcode of the collection
     */
    int hashCode();
}