package com.gitlab.marvinh.hugecollections.api;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.UnaryOperator;

/**
 * Created by chief on 21.03.17.
 */
public interface HugeList<V> extends HugeCollection<V> {
    /**
     * Adds the given element at the specified index into the list
     * @param index Index at which the element shall be added at
     * @param element Element to be added
     * @return True, if the element has been added. False if not.
     */
    boolean add(BigInteger index, V element);

    /**
     * Adds the given elements at the specified index into the list
     * @param index Index at which the element shall be added at
     * @param elements Element to be added
     * @return True, if the element has been added. False if not.
     */
    boolean addAll(BigInteger index, HugeCollection<V> elements);

    /**
     * @param index Index of the element to be returned. Index numbers start by 0
     * @return Element at the specified Index.
     */
    V get(BigInteger index);

    /**
     * @param element Element to seek
     * @return Index of the given element. Index numbers start by 0
     * @throws NoSuchElementException If the element does not exist in the list
     */
    BigInteger indexOf(V element);

    /**
     * @param element Element to seek
     * @return Index of the last occurrence of the specified element in this list
     * @throws NoSuchElementException If the element does not exist in the list
     */
    BigInteger lastIndexOf(V element);

    /**
     * @param index Index where to start
     * @return Iterator beginning to iterate at the given index
     */
    HugeCollectionIterator<V> iterator(BigInteger index);

    /**
     * Replaces each element of this list with the result of applying the operator to that element.
     * @param operator Operator to be applied to this list
     */
    void replaceAll(UnaryOperator<V> operator);

    /**
     * Replace element at the given index with the given element
     * @param index Index at which the given element should be put
     * @param element Element to put into this list
     * @return Element which has been replaced
     */
    V set(BigInteger index, V element);

    /**
     * Returns a part of this list as a new list
     * @param fromIndex Index where the sublist should start (inclusive)
     * @param toIndex Index where the sublist should end (exclusive)
     * @return The sublist
     */
    HugeList<V> subList(BigInteger fromIndex, BigInteger toIndex);

    /**
     * Transforms this list into a java.lang.List
     * @return The transformed list
     */
    List<V> toList();

    @Override
    default Collection<V> toCollection(){
        return toList();
    }
}
