package com.gitlab.marvinh.hugecollections.impl.stacks;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeStack;
import com.gitlab.marvinh.hugecollections.impl.lists.HugeWrappedArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 25.03.17.
 */
class HugeWrappedStackTest {
    private HugeStack<Integer> stack;

    @BeforeEach
    void setUp() {
        stack = new HugeWrappedStack<>();
    }

    @Test
    void pop() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(6, Math.toIntExact(stack.pop()));
        assertEquals(4, Math.toIntExact(stack.pop()));
        assertEquals(3, Math.toIntExact(stack.pop()));
        assertEquals(2, Math.toIntExact(stack.pop()));
        assertEquals(1, Math.toIntExact(stack.pop()));
    }

    @Test
    void push() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(Arrays.asList(
                6, 4, 3, 2, 1
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void toDeque() {
        Deque<Integer> deque = new ArrayDeque<>(Arrays.asList(
                6, 4, 3, 2, 1
        ));
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(new ArrayList<>(deque), new ArrayList<>(stack.toDeque()));
    }

    @Test
    void size() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(BigInteger.valueOf(5), stack.size());
    }

    @Test
    void isEmpty() {
        assertTrue(stack.isEmpty());
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertFalse(stack.isEmpty());
    }

    @Test
    void contains() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertTrue(stack.contains(6));
        assertTrue(stack.contains(4));
        assertTrue(stack.contains(1));
        assertFalse(stack.contains(7));
    }

    @Test
    void add() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void remove() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
        stack.remove(3);
        assertEquals(Arrays.asList(
                1, 2, 4, 6
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void containsAll() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        HugeWrappedArrayList<Integer> elements = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                2, 4
        )));
        assertTrue(stack.containsAll(elements));
    }

    @Test
    void addAll() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
        stack.addAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 6
        ))));
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6, 1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void removeAll() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
        stack.removeAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                3, 4
        ))));
        assertEquals(Arrays.asList(
                1, 2, 6
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void retainAll() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
        stack.retainAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                3, 4
        ))));
        assertEquals(Arrays.asList(
                3, 4
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void clear() {
        stack.add(1);
        stack.add(2);
        stack.add(3);
        stack.add(4);
        stack.add(6);
        assertEquals(Arrays.asList(
                1, 2, 3, 4, 6
        ), new ArrayList<>(stack.toCollection()));
        assertFalse(stack.isEmpty());
        stack.clear();
        assertTrue(stack.isEmpty());

    }

    @Test
    void toCollection() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(Arrays.asList(
                6, 4, 3, 2, 1
        ), new ArrayList<>(stack.toCollection()));
    }

    @Test
    void iterator() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        HugeCollectionIterator<Integer> iterator = stack.iterator();
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertFalse(iterator.hasNext());
    }

    @Test
    void peek() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(6);
        assertEquals(6, Math.toIntExact(stack.peek()));
        assertEquals(Arrays.asList(
                6, 4, 3, 2, 1
        ), new ArrayList<>(stack.toCollection()));
    }

}