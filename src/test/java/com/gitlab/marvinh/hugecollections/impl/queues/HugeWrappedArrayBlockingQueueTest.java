package com.gitlab.marvinh.hugecollections.impl.queues;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.impl.lists.HugeWrappedArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 06.04.17.
 */
class HugeWrappedArrayBlockingQueueTest {
    private HugeWrappedArrayBlockingQueue<Integer> queue;
    @BeforeEach
    void setUp() {
        queue = new HugeWrappedArrayBlockingQueue<>(BigInteger.TEN);
        queue.offer(1);
        queue.offer(3);
        queue.offer(4);
    }

    @Test
    void size() {
        assertTrue(queue.size().compareTo(BigInteger.valueOf(3)) == 0);
    }

    @Test
    void isEmpty() {
        assertFalse(queue.isEmpty());
        queue.clear();
        assertTrue(queue.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(queue.contains(1));
        assertTrue(queue.contains(3));
        assertTrue(queue.contains(4));
        assertFalse(queue.contains(0));
    }

    @Test
    void add() {
        queue.add(5);
        queue.add(7);
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                3,
                4,
                5,
                7
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void remove() {
        queue.remove(3);
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                4
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void containsAll() {
        assertTrue(queue.containsAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                4,
                1,
                3
        )))));
    }

    @Test
    void containsAll2() {
        assertTrue(queue.containsAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                4,
                1
        )))));
    }

    @Test
    void containsAll3() {
        assertFalse(queue.containsAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                4,
                1,
                3,
                0
        )))));
    }

    @Test
    void addAll() {
        queue.addAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1,
                3,
                4
        ))));
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                3,
                4,
                1,
                3,
                4
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void removeAll() {
        queue.removeAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1,
                3
        ))));
        assertEquals(new ArrayList<>(Collections.singletonList(
                4
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void retainAll() {
        queue.retainAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1,
                3
        ))));
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                3
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void clear() {
        assertFalse(queue.isEmpty());
        queue.clear();
        assertTrue(queue.isEmpty());
    }

    @Test
    void iterator() {
        HugeCollectionIterator<Integer> iterator = queue.iterator();
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertFalse(iterator.hasNext());
    }

    @Test
    void peek() {
        assertEquals(1, Math.toIntExact(queue.peek()));
        assertEquals(1, Math.toIntExact(queue.peek()));
    }

    @Test
    void offer() {
        queue.offer(5);
        queue.offer(0);
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                3,
                4,
                5,
                0
        )), new ArrayList<>(queue.toQueue()));
    }

    @Test
    void poll() {
        assertEquals(1, Math.toIntExact(queue.poll()));
        assertEquals(3, Math.toIntExact(queue.poll()));
        assertEquals(4, Math.toIntExact(queue.poll()));
    }

    @Test
    void toQueue() {
        assertEquals(new ArrayList<>(Arrays.asList(
                1,
                3,
                4
        )), new ArrayList<>(queue.toQueue()));
    }

}