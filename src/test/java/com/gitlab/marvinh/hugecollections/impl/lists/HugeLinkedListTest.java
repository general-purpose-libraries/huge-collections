package com.gitlab.marvinh.hugecollections.impl.lists;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 01.04.17.
 */
class HugeLinkedListTest {
    private HugeList<Integer> list;

    @BeforeEach
    void setUp() {
        list = new HugeLinkedList<>();
        list.add(1);
        list.add(4);
        list.add(2);
        list.add(3);
        list.add(5);
    }

    @Test
    void size() {
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
    }

    @Test
    void isEmpty() {
        assertFalse(list.isEmpty());
        list = new HugeLinkedList<>();
        assertTrue(list.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertFalse(list.contains(0));
        assertFalse(list.contains(6));
    }

    @Test
    void add() {
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertFalse(list.contains(0));
        assertFalse(list.contains(6));
        assertTrue(list.add(6));
        assertTrue(list.size().compareTo(BigInteger.valueOf(6)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertFalse(list.contains(0));
        assertTrue(list.contains(6));
        assertTrue(list.add(0));
        assertTrue(list.size().compareTo(BigInteger.valueOf(7)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertTrue(list.contains(0));
        assertTrue(list.contains(6));
    }

    @Test
    void add2() {
        list = new HugeLinkedList<>();
        list.add(BigInteger.ZERO, 2);
        assertTrue(list.size().compareTo(BigInteger.ONE) == 0);
        assertTrue(list.contains(2));
    }

    @Test
    void remove(){
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertFalse(list.contains(0));
        assertFalse(list.contains(6));
        assertThrows(NoSuchElementException.class, () -> list.remove(0));
    }

    @Test
    void remove2() {
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertTrue(list.contains(5));
        assertFalse(list.contains(0));
        assertFalse(list.contains(6));
        assertTrue(list.remove(5));
        assertTrue(list.size().compareTo(BigInteger.valueOf(4)) == 0);
        assertTrue(list.contains(4));
        assertTrue(list.contains(1));
        assertTrue(list.contains(2));
        assertTrue(list.contains(3));
        assertFalse(list.contains(5));
        assertFalse(list.contains(0));
        assertFalse(list.contains(6));
    }

    @Test
    void containsAll() {
        assertTrue(list.containsAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                5, 4, 3, 2, 1
        )))));
    }

    @Test
    void addAll() {
        list.addAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                5, 4, 3, 2, 1
        ))));
        assertEquals(new HugeLinkedList<>(new ArrayList<>(Arrays.asList(
                1, 4, 2, 3, 5, 5, 4, 3, 2, 1
        ))), list);
    }

    @Test
    void removeAll() {
        list.removeAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                4, 3, 2
        ))));
        assertEquals(new HugeLinkedList<>(new ArrayList<>(Arrays.asList(
                1, 5
        ))), list);
    }

    @Test
    void retainAll() {
        list.retainAll(new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                4, 3, 2
        ))));
        assertEquals(new HugeLinkedList<>(new ArrayList<>(Arrays.asList(
                4, 2, 3
        ))), list);
    }

    @Test
    void clear() {
        assertFalse(list.isEmpty());
        assertTrue(list.size().compareTo(BigInteger.ZERO) > 0);
        list.clear();
        assertTrue(list.isEmpty());
        assertTrue(list.size().compareTo(BigInteger.ZERO) == 0);
    }

    @Test
    void iterator() {
        HugeCollectionIterator<Integer> iterator = list.iterator();
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(5, Math.toIntExact(iterator.next()));
        assertFalse(iterator.hasNext());
    }

    @Test
    void get() {
        assertEquals(4, Math.toIntExact(list.get(BigInteger.ONE)));
        assertEquals(1, Math.toIntExact(list.get(BigInteger.ZERO)));
        assertEquals(3, Math.toIntExact(list.get(BigInteger.valueOf(3))));
        assertEquals(2, Math.toIntExact(list.get(BigInteger.valueOf(2))));
        assertEquals(5, Math.toIntExact(list.get(BigInteger.valueOf(4))));
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(BigInteger.valueOf(5)));
    }

    @Test
    void indexOf() {
        list.add(4);
        assertEquals(BigInteger.ONE, list.indexOf(4));
        assertEquals(BigInteger.ZERO, list.indexOf(1));
        assertEquals(BigInteger.valueOf(3), list.indexOf(3));
        assertEquals(BigInteger.valueOf(2), list.indexOf(2));
        assertEquals(BigInteger.valueOf(4), list.indexOf(5));
        assertThrows(NoSuchElementException.class, () -> list.indexOf(6));
    }

    @Test
    void lastIndexOf() {
        list.add(4);
        assertEquals(BigInteger.valueOf(5), list.lastIndexOf(4));
        assertEquals(BigInteger.ZERO, list.lastIndexOf(1));
        assertEquals(BigInteger.valueOf(3), list.lastIndexOf(3));
        assertEquals(BigInteger.valueOf(2), list.lastIndexOf(2));
        assertEquals(BigInteger.valueOf(4), list.lastIndexOf(5));
        assertThrows(NoSuchElementException.class, () -> list.lastIndexOf(6));
    }

    @Test
    void replaceAll() {
        list.replaceAll(integer -> {
            if (integer == 4){
                return 2;
            }
            return integer;
        });
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                1,
                2,
                2,
                3,
                5
        ))), list);
    }

    @Test
    void set() {
        list.set(BigInteger.ZERO, 4);
        list.set(BigInteger.ONE, 3);
        list.set(BigInteger.valueOf(4), 2);
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                4,
                3,
                2,
                3,
                2
        ))), list);
    }

    @Test
    void subList() {
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                1,
                4,
                2,
                3,
                5
        ))), list);
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                1
        ))), list.subList(BigInteger.ZERO, BigInteger.ONE));
        assertEquals(new HugeLinkedList(new ArrayList(

        )), list.subList(BigInteger.ZERO, BigInteger.ZERO));
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                4,
                2,
                3
        ))), list.subList(BigInteger.ONE, BigInteger.valueOf(4)));
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                4,
                2,
                3,
                5
        ))), list.subList(BigInteger.ONE, BigInteger.valueOf(5)));
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                1,
                4,
                2,
                3,
                5
        ))), list.subList(BigInteger.ZERO, BigInteger.valueOf(5)));
    }

    @Test
    void toList() {
        assertEquals(new HugeLinkedList(new ArrayList(Arrays.asList(
                1,
                4,
                2,
                3,
                5
        ))), list);
        assertEquals(new LinkedList<>(Arrays.asList(
                1,
                4,
                2,
                3,
                5
        )), list.toList());
    }

}