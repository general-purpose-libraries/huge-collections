package com.gitlab.marvinh.hugecollections.impl.iterators;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 25.03.17.
 */
class LimitedSizeCollectionIteratorTest {
    private HugeCollectionIterator<Integer> iterator;

    @BeforeEach
    void setUp() {
        iterator = new LimitedSizeCollectionIterator<>(new ArrayList<>(Arrays.asList(
                1, 3, 2, 4, 6, 5
        )));
    }

    @Test
    void hasPrevious() {
        assertFalse(iterator.hasPrevious());
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(5, Math.toIntExact(iterator.next()));

        assertTrue(iterator.hasPrevious());
        iterator.previous();
        assertTrue(iterator.hasPrevious());
        iterator.previous();
        assertTrue(iterator.hasPrevious());
        iterator.previous();
        assertTrue(iterator.hasPrevious());
        iterator.previous();
        assertTrue(iterator.hasPrevious());
        iterator.previous();
        assertFalse(iterator.hasPrevious());
    }

    @Test
    void previous() {
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(5, Math.toIntExact(iterator.next()));

        assertEquals(6, Math.toIntExact(iterator.previous()));
        assertEquals(4, Math.toIntExact(iterator.previous()));
        assertEquals(2, Math.toIntExact(iterator.previous()));
        assertEquals(3, Math.toIntExact(iterator.previous()));
        assertEquals(1, Math.toIntExact(iterator.previous()));
    }

    @Test
    void reverse() {
        iterator = iterator.reverse();
        assertEquals(5, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(1, Math.toIntExact(iterator.next()));
    }

    @Test
    void reverse2() {
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        iterator = iterator.reverse();
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(1, Math.toIntExact(iterator.next()));
    }

    @Test
    void remove() {
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        iterator.remove();
        assertEquals(1, Math.toIntExact(iterator.previous()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(5, Math.toIntExact(iterator.next()));
    }

    @Test
    void hasNext() {
        assertTrue(iterator.hasNext());
        iterator.next();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertTrue(iterator.hasNext());
        iterator.next();
        assertFalse(iterator.hasNext());
    }

    @Test
    void next() {
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertEquals(5, Math.toIntExact(iterator.next()));
    }

}