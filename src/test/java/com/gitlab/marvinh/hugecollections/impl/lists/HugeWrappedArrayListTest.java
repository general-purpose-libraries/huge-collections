package com.gitlab.marvinh.hugecollections.impl.lists;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import com.gitlab.marvinh.hugecollections.api.HugeList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 25.03.17.
 */
class HugeWrappedArrayListTest {
    private HugeList<Integer> list;

    @BeforeEach
    void setUp() {
        list = new HugeWrappedArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(6);
    }

    @Test
    void size() {
        assertTrue(list.size().compareTo(BigInteger.valueOf(5)) == 0);
    }

    @Test
    void isEmpty() {
        assertFalse(list.isEmpty());
        list = new HugeWrappedArrayList<>();
        assertTrue(list.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(list.contains(3));
        assertTrue(list.contains(6));
        assertFalse(list.contains(5));
    }

    @Test
    void add() {
        assertFalse(list.contains(7));
        list.add(7);
        assertTrue(list.contains(7));
    }

    @Test
    void remove() {
        assertTrue(list.contains(6));
        list.remove(6);
        assertFalse(list.contains(6));
    }

    @Test
    void containsAll() {
        HugeList<Integer> elemensToContain = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 3, 4, 6
        )));
        assertTrue(list.containsAll(elemensToContain));
        HugeList<Integer> elemensToContain2 = new HugeWrappedArrayList<>(elemensToContain);
        elemensToContain2.add(7);
        assertFalse(list.containsAll(elemensToContain2));
    }

    @Test
    void addAll() {
        HugeList<Integer> elemensToAdd = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                5, 7, 8, 9
        )));
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 6
        ))));
        list.addAll(elemensToAdd);
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 6, 5, 7, 8, 9
        ))));
    }

    @Test
    void removeAll() {
        HugeList<Integer> elemensToRemove = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                2, 6, 3
        )));
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 6
        ))));
        list.removeAll(elemensToRemove);
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 4
        ))));
    }

    @Test
    void retainAll() {
        HugeList<Integer> elemensToRetain = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                2, 6, 3
        )));
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 4, 6
        ))));
        list.retainAll(elemensToRetain);
        assertEquals(list, new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                2, 3, 6
        ))));
    }

    @Test
    void clear() {
        assertFalse(list.isEmpty());
        assertTrue(list.size().compareTo(BigInteger.ZERO) > 0);
        list.clear();
        assertTrue(list.isEmpty());
        assertTrue(list.size().compareTo(BigInteger.ZERO) == 0);
    }

    @Test
    void iterator() {
        HugeCollectionIterator<Integer> iterator = list.iterator();
        assertEquals(1, Math.toIntExact(iterator.next()));
        assertEquals(2, Math.toIntExact(iterator.next()));
        assertEquals(3, Math.toIntExact(iterator.next()));
        assertEquals(4, Math.toIntExact(iterator.next()));
        assertEquals(6, Math.toIntExact(iterator.next()));
        assertFalse(iterator.hasNext());
    }

    @Test
    void get() {
        assertEquals(1, Math.toIntExact(list.get(BigInteger.ZERO)));
        assertEquals(2, Math.toIntExact(list.get(BigInteger.ONE)));
        assertEquals(3, Math.toIntExact(list.get(BigInteger.valueOf(2))));
        assertEquals(4, Math.toIntExact(list.get(BigInteger.valueOf(3))));
        assertEquals(6, Math.toIntExact(list.get(BigInteger.valueOf(4))));
    }

    @Test
    void indexOf() {
        assertEquals(BigInteger.ZERO, list.indexOf(1));
        assertEquals(BigInteger.ONE, list.indexOf(2));
        assertEquals(BigInteger.valueOf(2), list.indexOf(3));
        assertEquals(BigInteger.valueOf(3), list.indexOf(4));
        assertEquals(BigInteger.valueOf(4), list.indexOf(6));
    }

    @Test
    void lastIndexOf() {
        assertEquals(BigInteger.ZERO, list.lastIndexOf(1));
        assertEquals(BigInteger.ONE, list.lastIndexOf(2));
        assertEquals(BigInteger.valueOf(2), list.lastIndexOf(3));
        assertEquals(BigInteger.valueOf(3), list.lastIndexOf(4));
        assertEquals(BigInteger.valueOf(4), list.lastIndexOf(6));
    }

    @Test
    void replaceAll() {
        list.replaceAll(integer -> {
            if (integer.equals(4)){
                return 7;
            }
            return integer;
        });
        HugeList<Integer> newList = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                1, 2, 3, 7, 6
        )));
        assertEquals(newList, list);
    }

    @Test
    void set() {
        list.set(BigInteger.ZERO, 9);
        list.set(BigInteger.valueOf(2), 7);
        list.set(BigInteger.valueOf(3), 7);
        HugeList<Integer> newList = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                9, 2, 7, 7, 6
        )));
        assertEquals(newList, list);
    }

    @Test
    void subList() {
        HugeList<Integer> newList = new HugeWrappedArrayList<>(new ArrayList<>(Arrays.asList(
                2, 3
        )));
        assertEquals(newList, list.subList(BigInteger.ONE, BigInteger.valueOf(3)));
    }

    @Test
    void toList() {
        List<Integer> list = Arrays.asList(
                1, 2, 3, 4, 6
        );
        assertEquals(list, this.list.toList());
    }

    @Test
    void equals() {
        HugeList<Integer> list = new HugeWrappedArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(6);
        assertTrue(this.list.equals(list));
    }
}