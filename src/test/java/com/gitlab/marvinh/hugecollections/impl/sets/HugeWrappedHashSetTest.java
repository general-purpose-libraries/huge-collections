package com.gitlab.marvinh.hugecollections.impl.sets;

import com.gitlab.marvinh.hugecollections.api.HugeCollectionIterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 25.03.17.
 */
class HugeWrappedHashSetTest {
    private HugeWrappedHashSet<Integer> set;

    @BeforeEach
    void setUp() {
        set = new HugeWrappedHashSet<>(Arrays.asList(
                1, 2, 3, 4, 6
        ));
    }

    @Test
    void size() {
        assertEquals(BigInteger.valueOf(5), set.size());
    }

    @Test
    void isEmpty() {
        assertFalse(set.isEmpty());
        set.clear();
        assertTrue(set.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(set.contains(4));
        assertFalse(set.contains(5));
    }

    @Test
    void add() {
        set.add(8);
        assertEquals(new HashSet<>(Arrays.asList(
                1, 2, 3, 4, 6, 8
        )), set.toSet());
    }

    @Test
    void remove() {
        set.remove(3);
        assertEquals(new HashSet<>(Arrays.asList(
                1, 2, 4, 6
        )), set.toSet());
    }

    @Test
    void containsAll() {
        assertTrue(set.containsAll(new HugeWrappedHashSet<>(Arrays.asList(
                1, 2, 3
        ))));
    }

    @Test
    void addAll() {
        set.addAll(new HugeWrappedHashSet<>(Arrays.asList(
                3, 7, 8
        )));
        assertEquals(new HashSet<>(Arrays.asList(
                1, 2, 3, 4, 6, 7, 8
        )), set.toSet());
    }

    @Test
    void removeAll() {
        set.removeAll(new HugeWrappedHashSet<>(Arrays.asList(
                1, 2, 3
        )));
        assertEquals(new HashSet<>(Arrays.asList(
                4, 6
        )), set.toSet());
    }

    @Test
    void retainAll() {
        set.retainAll(new HugeWrappedHashSet<>(Arrays.asList(
                2, 3
        )));
        assertEquals(new HashSet<>(Arrays.asList(
                2, 3
        )), set.toSet());
    }

    @Test
    void clear() {
        assertEquals(new HashSet<>(Arrays.asList(
                1, 2, 3, 4, 6
        )), set.toSet());
        assertFalse(set.isEmpty());
        set.clear();
        assertTrue(set.isEmpty());
    }

    @Test
    void iterator() {
        Set<Integer> valueSet = new HashSet<>(Arrays.asList(
                1, 2, 3, 4, 6
        ));
        HugeCollectionIterator<Integer> iterator = set.iterator();
        int element = iterator.next();
        assertTrue(valueSet.contains(element));
        valueSet.remove(element);
        element = iterator.next();
        assertTrue(valueSet.contains(element));
        valueSet.remove(element);
        element = iterator.next();
        assertTrue(valueSet.contains(element));
        valueSet.remove(element);
        element = iterator.next();
        assertTrue(valueSet.contains(element));
        valueSet.remove(element);
        element = iterator.next();
        assertTrue(valueSet.contains(element));
        valueSet.remove(element);
        assertFalse(iterator.hasNext());
    }

    @Test
    void toSet() {
        assertEquals(new HashSet<>(Arrays.asList(
                1, 2, 3, 4, 6
        )), set.toSet());
    }

}