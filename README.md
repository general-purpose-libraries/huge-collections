# Copyright note and licensing information
Copyright &copy; Chief of Zetten, 21.03.2017<br>
This library is licensed under the terms of the [LGPL, Version 3](LICENSE.txt).
A copy of the license is distributed with this software.

# Purpose
This library is indented to be used in applications using collections
with no technically limited size.
For users thinking about later migration to HugeCollections, but currently
want to keep using java collections, there exists
some wrapper classes which adapt java collection classes to HugeCollections.

# Installation
Just download the software from the
[project website](https://gitlab.com/general-purpose-libraries/huge-collections)
and then add it to your Java project. It you are using maven, you only have to add it as a dependency:
```
<groupId>com.gitlab.marvinh</groupId>
<artifactId>huge-collections</artifactId>
<version>1.0</version>
```

# Usage
The API contains following interfaces:
* __Algorithm__: If you want to do operations for each element in a collection,
you can use an implementation of this interface in the __apply()__ method of
the collection
* __HugeCollection__: If you want to declare a collection in your code, but you 
do not want to specify the exact type of the collection, you can use this interface.
* __HugeList__: This interface describes collections which elements are stored ordered.
It can contain multiple entries with the same elements.
* __HugeQueue__: Describes a FIFO buffer. This means elements that have been added first
are taken out first.
* __HugeSet__: A set of elements. It does not allow duplicate elements and does not guarantee
an order in which elements are retrieved.
* __HugeStack__: A stack where elements having been added first are retrieved last.

There exist some implementations wrapping java collections into HugeCollections. The size of
these wrappers is limited to Integer.MAX_VALUE elements, because correct functionality is not
garanteed when the related java collections have more
than this amount of elements. The wrapper classes are named by the pattern
```
HugeWrapped*
```
Where "*" is the name of the wrapped java collection class.

# Contact
If you wish to contact me, you can send me a message at [irc.freenode.net](irc://chiefofzetten@irc.freenode.net) or
write an email to [computer-science-blog@web.de](mailto:computer-science-blog@web.de).